-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 03 jan. 2021 à 20:38
-- Version du serveur :  5.7.26
-- Version de PHP :  7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bibliotheque`
--

-- --------------------------------------------------------

--
-- Structure de la table `est_ami`
--

DROP TABLE IF EXISTS `est_ami`;
CREATE TABLE IF NOT EXISTS `est_ami` (
  `id` int(11) NOT NULL,
  `id_utilisateur` int(11) NOT NULL,
  PRIMARY KEY (`id`,`id_utilisateur`),
  KEY `est_ami_utilisateur0_FK` (`id_utilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `est_ami`
--

INSERT INTO `est_ami` (`id`, `id_utilisateur`) VALUES
(2, 1),
(3, 1),
(1, 2),
(4, 2),
(1, 3),
(2, 4);

-- --------------------------------------------------------

--
-- Structure de la table `livre`
--

DROP TABLE IF EXISTS `livre`;
CREATE TABLE IF NOT EXISTS `livre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(120) NOT NULL,
  `auteur` varchar(120) NOT NULL,
  `type_de_support` enum('livre','BD','manga','revue','autre') NOT NULL,
  `edition` varchar(120) NOT NULL,
  `date_de_publication` date NOT NULL,
  `ISBN` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `livre`
--

INSERT INTO `livre` (`id`, `titre`, `auteur`, `type_de_support`, `edition`, `date_de_publication`, `ISBN`) VALUES
(1, 'Harry Potter', 'J.K. Rowling', 'livre', 'Gallimard', '1998-10-12', 9782070541270),
(2, 'Voyage au bout de la nuit', 'Céline', 'livre', 'Folio', '1972-02-16', 9782070360284),
(3, 'A la recherche du temps perdu', 'Marcel Proust', 'livre', 'Quarto', '1999-02-23', 9782070754922),
(4, '1984', 'George Orwell', 'livre', 'Gallimard', '1972-11-16', 9782070368228),
(5, 'L\'étranger', 'Albert Camus', 'livre', 'Folio', '1972-01-07', 9782070360024),
(6, 'Les misérables', 'Victor Hugo', 'livre', 'Classiques', '2019-01-09', 9782211238465),
(7, 'Orgueil et préjugés', 'Jane Austen', 'livre', 'Litterature Etrangere', '2012-01-05', 9782264058249),
(8, 'Madame Bovary', 'Gustave Flaubert', 'livre', 'Folio Classique', '2001-05-16', 9782070413119),
(11, 'La Peste', 'Camus', 'livre', 'Gallimard', '2001-09-30', 890174582),
(12, 'L\'arbre monde', 'Richard Power', 'livre', 'W  W Norton and Company', '2019-09-30', 9782264074430),
(13, 'La nature de l\'espace et du temps', 'Stephen Hawking et Roger Penrose', 'livre', 'Folio Essais', '1996-12-31', 9782070429271),
(14, 'Confessions Tome 1', 'Rousseau', 'livre', 'Brodard et Taupin', '1971-12-31', 3033109801),
(21, 'Harry Potter et la chambre des secrets', 'J.K. Rowling', 'livre', 'Gallimard', '1999-10-09', 9782070541294),
(27, 'Présent et avenir', 'C.G. Jung', 'livre', 'Denoël', '1962-01-31', 2282300653),
(28, 'Le gai savoir', 'Nietzsche', 'livre', 'Flammarion', '1997-10-09', 2080707183),
(29, 'La conquête du bonheur', 'Bertrand Russell', 'livre', 'Payot', '1962-10-09', 9782228894524),
(30, 'Oeuvres 1', 'Spinoza', 'livre', 'Garnier Flammarion', '1965-01-09', 5090);

-- --------------------------------------------------------

--
-- Structure de la table `possede`
--

DROP TABLE IF EXISTS `possede`;
CREATE TABLE IF NOT EXISTS `possede` (
  `id_livre` int(11) NOT NULL,
  `id_utilisateur` int(11) NOT NULL,
  KEY `possede_utilisateur0_FK` (`id_utilisateur`,`id_livre`),
  KEY `id_possede` (`id_livre`,`id_utilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `possede`
--

INSERT INTO `possede` (`id_livre`, `id_utilisateur`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(29, 1),
(30, 1),
(1, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(1, 3),
(12, 3),
(13, 3),
(14, 3),
(1, 4),
(15, 4),
(16, 4),
(21, 4),
(27, 4),
(2, 5),
(28, 5),
(29, 5),
(30, 5);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(120) NOT NULL,
  `prenom` varchar(120) NOT NULL,
  `pseudo` varchar(120) NOT NULL,
  `mot_de_passe` varchar(120) NOT NULL,
  PRIMARY KEY (`id_utilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id_utilisateur`, `nom`, `prenom`, `pseudo`, `mot_de_passe`) VALUES
(1, 'Brisorgueil', 'Elodie', 'Didi22', 'azerty'),
(2, 'Houeix', 'Camille', 'Camou', 'abcdef'),
(3, 'Tréhorel', 'Lhuckas', 'Lulu', '123456'),
(4, 'Gauvry', 'Emilie', 'Mimi cracra', 'azerty123');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
