package net.csc.bibliotheque;

import java.io.IOException;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import com.dieselpoint.norm.Database;


public class SecondaryController {
	
	@FXML
	private VBox list;
	
	@FXML
	private Label titre;
	
	@FXML
	private VBox Vbox3;
	
	@FXML
	private Button btn3;
	
	@FXML
	private Button btn4;
	
	@FXML
	private Button btn5;
	
	@FXML
	private Button btn6;
	
	@FXML
	private Button btn7;
	
    @FXML
    private void switchToPrimary() throws IOException {
        App.setRoot("primary");
    }
}