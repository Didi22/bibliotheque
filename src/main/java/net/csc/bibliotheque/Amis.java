package net.csc.bibliotheque;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.*;

@Table(name = "est_ami")
public class Amis {
	
	//class with the data of a friend
	@Id
	@GeneratedValue
	public Integer id;
	
	public Integer id_utilisateur;
	
	public Integer id_ami;
	
	public String statut;

}
