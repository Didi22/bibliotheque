package net.csc.bibliotheque;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.awt.Color;
import java.awt.Image;
import java.io.FileInputStream;
import java.io.IOException;

import com.dieselpoint.norm.Database;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;
    private static Database db;
    @FXML
    ImageView image = new ImageView();
    
    //Connection instance MySQL
    public static Database getDB() {
    	if(db == null) {
			db = new Database();
			db.setJdbcUrl("jdbc:mysql://localhost/bibliotheque?serverTimezone=UTC");
			db.setUser("root");
			db.setPassword("");
    	}
    	return db;
	}
    
    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("primary"), 620, 460);
        stage.setTitle("Bibliothèque"); 
        stage.setScene(scene);
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}