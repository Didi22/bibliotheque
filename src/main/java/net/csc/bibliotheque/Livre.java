package net.csc.bibliotheque;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.*;

	
@Table(name = "livre")
public class Livre {
	
	//class with the data of a book
	private Integer id;
	
	@Id
	@GeneratedValue
	public Integer getId () {
		return this.id;
	}
	@Id
	@GeneratedValue
	public void setId (Integer id) {
		this.id = id;
	}
	
	public String titre;
	
	public String auteur;
	
	@Enumerated(EnumType.STRING)
	public String type_de_support;
	
	public String edition;
	
	public Date date_de_publication;
	
	public Integer ISBN;

}
