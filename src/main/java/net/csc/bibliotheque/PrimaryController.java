package net.csc.bibliotheque;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.Panel;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.plaf.OptionPaneUI;

import com.dieselpoint.norm.Database;
import com.dieselpoint.norm.Query;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;


public class PrimaryController {
	
	@FXML
	ImageView image = new ImageView();
	
	@FXML
	public static VBox Vbox;
	
	@FXML
	public Label lbl;
	
	@FXML
	public VBox Vbox2;
	
	public VBox list;
	
	@FXML
	public Button btn3, btn4, btn5, btn6, btn7, btn, btnIns;
	
	@FXML
	public Label lbl1, lbl2;
	
	@FXML
	public TextField textfield1;
	
	@FXML
	public PasswordField passwordfield;
	
	public String name;
	
	public Utilisateur newUser, newAmi, user, user2;
	
	public Livre newLivre;
	
	public Amis newAmitie, nouvelleAmitie;
	
	public List<Livre> listLivres;
	
	public Possede possession, newPossede;
		
	//method executed when loading the file
	@FXML
	public void handle(ActionEvent event) {
		Button cliqued = (Button)event.getSource();
		String l = cliqued.getText();
		Vbox2.setSpacing(3);
		name = textfield1.getText();
		listLivres = App.getDB().sql("SELECT * FROM livre JOIN possede ON possede.id_livre = livre.id JOIN utilisateur ON utilisateur.id = possede.id_utilisateur WHERE pseudo LIKE ?", name).results(Livre.class);
		List<Utilisateur> amis = App.getDB().sql("SELECT u2.nom, u2.prenom, u2.pseudo FROM utilisateur AS u1 JOIN est_ami ON est_ami.id_utilisateur = u1.id JOIN utilisateur AS u2 ON u2.id = est_ami.id_ami WHERE u1.pseudo LIKE ? AND statut = 'ami'", name).results(Utilisateur.class);
			
		//where the user enters his username and password and clicks on the button
		if(l.equals("Validez")) {
			String motPasse = passwordfield.getText();
			user = App.getDB().where("pseudo LIKE ? AND mot_de_passe LIKE ?", name, motPasse).first(Utilisateur.class);
			
			if (name.trim().isEmpty() || motPasse.trim().isEmpty()) {
				System.out.println("pseudo et/ou mot de passe erroné(s)");
			}
			else if (user == null) {
				System.out.println("pseudo et/ou mot de passe erroné(s)");
			}
			else if (user !=  null) {
			    lbl.setText("Ma bibliothèque");
			    Vbox2.getChildren().clear();
			    
			    for(Livre livre : listLivres) {
			    	Label lbl4 = new Label();
			    	lbl4.setText((livre.titre) + " /\t" + (livre.auteur)  + " /\t" + (livre.edition) + " /\t" + (livre.ISBN) + " /\t" + (livre.date_de_publication));
			    	Vbox2.getChildren().add(lbl4);
			    }	
			    btn3.setVisible(true);
				btn4.setVisible(true);
				btn5.setVisible(true);
				btn6.setVisible(true);
				btn7.setVisible(true);
			}
		}
		
		//where the user subscribes
		if(l.equals("Inscription")) {
			
			btn3.setVisible(false);
			btn4.setVisible(false);
			btn5.setVisible(false);
			btn6.setVisible(false);
			btn7.setVisible(false);
			
		    lbl.setText("Inscription");
		    Vbox2.getChildren().clear();
		    
		    Label label = new Label();
		    label.setText("Nom");
		    TextField textfield = new TextField();
		    Label label1 = new Label();
		    label1.setText("Prénom");
		    TextField textfield1 = new TextField();
		    Label label2 = new Label();
		    label2.setText("Pseudo");
		    TextField textfield2 = new TextField();
		    Label label3 = new Label();
		    label3.setText("Mot de passe");
		    TextField textfield3 = new TextField();
		    Label label4 = new Label();
		    label4.setText("Confirmation mot de passe");
		    TextField textfield4 = new TextField();
		    Button btnI = new Button();
		    
		    btnI.setText("S'inscrire");
		    btnI.setOnAction(new EventHandler<ActionEvent>() {
		    	@Override
		    	public void handle(ActionEvent event) {	    
    			    newUser = new Utilisateur();
    			    newUser.nom = textfield.getText();
    			    newUser.prenom = textfield1.getText();
    			    newUser.pseudo = textfield2.getText();
    			    newUser.mot_de_passe = textfield3.getText();
    			    App.getDB().insert(newUser);
    			    JOptionPane.showMessageDialog(null, "Vous êtes inscrit", "inscription", JOptionPane.WARNING_MESSAGE);
    			    btn3.setVisible(true);
    				btn4.setVisible(true);
    				btn5.setVisible(true);
    				btn6.setVisible(true);
    				btn7.setVisible(true);
		    	}
		    });
		    Vbox2.getChildren().addAll(label, textfield, label1, textfield1, label2, textfield2, label3, textfield3, label4, textfield4, btnI);
		}
		
		//where the user visualizes the list of his friends and their libraries
		if(l.equals("Mes amis")) {
		    lbl.setText("Mes amis");
		    Vbox2.getChildren().clear();
		    
		    for(Utilisateur utilisateur : amis) {
		    	Label lbl5 = new Label();
		    	lbl5.setText((utilisateur.nom) + "\t" + (utilisateur.prenom)  + "\talias " + (utilisateur.pseudo));
		    	Button btn = new Button();
		    	
		    	btn.setText("Sa bibliothèque");			    	 
		    	btn.setOnAction(new EventHandler<ActionEvent>() {
			    	@Override
			    	public void handle(ActionEvent event) {	    
	    			    lbl.setText("Bibliothèque de  " + utilisateur.prenom);
	    			    Vbox2.getChildren().clear();
	    			    
	    			    String ami = utilisateur.pseudo;
	    				List<Livre> listLivresAmi = App.getDB().sql("SELECT * FROM livre JOIN possede ON possede.id_livre = livre.id JOIN utilisateur ON utilisateur.id = possede.id_utilisateur WHERE pseudo LIKE ?", ami).results(Livre.class);

	    			    for(Livre livre : listLivresAmi) {
	    			    	Label lblA = new Label();
	    			    	lblA.setText((livre.titre) + " /\t" + (livre.auteur)  + " /\t" + (livre.edition) + " /\t" + (livre.ISBN) + " /\t" + (livre.date_de_publication));
	    			    	Vbox2.getChildren().add(lblA);
	    			    }
			    	}
			    });
//		    	Button btn2 = new Button();
//		    	
//		    	btn2.setText("Supprimer");
//		    	btn2.setOnKeyPressed((new EventHandler<KeyEvent>() {
//			    	@Override
//			    	public void handle(KeyEvent event) {	
//			    		App.getDB().sql("DELETE FROM est_ami WHERE id_utilisateur = ? AND id = ?", id_utilisateur, id).execute();
//			    	} 
//			    }));
		    	
		    	Vbox2.getChildren().addAll(lbl5, btn /*,btn2*/);
		    }	
		}
			
		//where the user views the list of his books
		if(l.equals("Ma bibliothèque")) {
		    lbl.setText("Ma bibliothèque");
		    Vbox2.getChildren().clear();
		   
		    for(Livre livre : listLivres) {
		    	Label lbl6 = new Label();
		    	lbl6.setText((livre.titre) + " /\t" + (livre.auteur)  + " /\t" + (livre.edition) + " /\t" + (livre.ISBN) + " /\t" + (livre.date_de_publication));
		    	
		    	Button btn = new Button();
		    	btn.setText("Supprimer");
		    	btn.setOnAction(new Click(user.id, livre.getId()));
		    	Vbox2.getChildren().addAll(lbl6, btn);
		    }	
		}
		
		//where the user saves his books
		if(l.equals("Recherche livre")) {
		    lbl.setText("Recherche livre");
		    Vbox2.getChildren().clear();
		    
		    String tab[] = { "livre", "BD", "manga", "revue", "autre"};
		   
		    Label label = new Label();
		    label.setText("Titre");
		    TextField textfield = new TextField();
		    
		    list = new VBox();
    		ScrollPane scrollpane = new ScrollPane();
    		list.getChildren().clear();
    				    
		    textfield.setOnKeyPressed((new EventHandler<KeyEvent>() {
		    	@Override
		    	public void handle(KeyEvent event) {	
		    		Label labelTitre;
		    		String saisie = textfield.getText();
		    		List<Livre> listTitres;
		    		scrollpane.setContent(list);
		    		list.getChildren().clear();
		    		
		    		if(saisie.trim().isEmpty()) {
		    			listTitres = App.getDB().sql("SELECT titre FROM livre").results(Livre.class);
		    		} 
		    		else {
		    			listTitres = App.getDB().sql("SELECT titre FROM livre WHERE titre LIKE ?", '%' + saisie + '%').results(Livre.class);
		    		}
		    		
		    		for(Livre livre : listTitres) {
		    			labelTitre = new Label();
		    			labelTitre.setText(String.valueOf(livre.titre));
		    			list.getChildren().add(labelTitre);
		    		}
		    	} 
		    }));
		    
		    Label label2 = new Label();
		    label2.setText("Auteur");
		    TextField textfield2 = new TextField();
		    textfield2.setOnKeyPressed((new EventHandler<KeyEvent>() {
		    	@Override
		    	public void handle(KeyEvent event) {	
		    		Label labelAuteurs;
		    		String saisie2 = textfield2.getText();
		    		List<Livre> listAuteurs;
		    		scrollpane.setContent(list);
		    		list.getChildren().clear();
		    		
		    		if(saisie2.trim().isEmpty()) {
		    			listAuteurs = App.getDB().sql("SELECT auteur FROM livre").results(Livre.class);
		    		} 
		    		else {
		    			listAuteurs = App.getDB().sql("SELECT auteur FROM livre WHERE auteur LIKE ?", '%' + saisie2 + '%').results(Livre.class);
		    		}
		    		
		    		for(Livre livre : listAuteurs) {
		    			labelAuteurs = new Label();
		    			labelAuteurs.setText(String.valueOf(livre.auteur));
		    			list.getChildren().add(labelAuteurs);
		    			
		    		}
		    	} 
		    }));
	
		    Label label3 = new Label();
		    label3.setText("Type de support");
		    ComboBox format = new ComboBox();
		    format.getItems().setAll(tab);
		    Label label4 = new Label();
		    label4.setText("Edition");
		    TextField textfield4 = new TextField();
		    textfield4.setOnKeyPressed((new EventHandler<KeyEvent>() {
		    	@Override
		    	public void handle(KeyEvent event) {	
		    		Label labelEditions;
		    		String saisie4 = textfield4.getText();
		    		List<Livre> listEditions;
		    		scrollpane.setContent(list);
		    		list.getChildren().clear();
		    		
		    		if(saisie4.trim().isEmpty()) {
		    			listEditions = App.getDB().sql("SELECT edition FROM livre").results(Livre.class);
		    		} 
		    		else {
		    			listEditions = App.getDB().sql("SELECT edition FROM livre WHERE edition LIKE ?", '%' + saisie4 + '%').results(Livre.class);
		    		}
		    		
		    		for(Livre livre : listEditions) {
		    			labelEditions = new Label();
		    			labelEditions.setText(String.valueOf(livre.edition));
		    			list.getChildren().add(labelEditions);
		    			
		    		}
		    	} 
		    }));
		    
		    Label label5 = new Label();
		    label5.setText("ISBN");
		    TextField textfield5 = new TextField();
		    textfield5.setOnKeyPressed((new EventHandler<KeyEvent>() {
		    	@Override
		    	public void handle(KeyEvent event) {	
		    		Label labelISBN;
		    		String saisie5 = textfield5.getText();
		    		List<Livre> listISBN;
		    		scrollpane.setContent(list);
		    		list.getChildren().clear();
		    		
		    		if(saisie5.trim().isEmpty()) {
		    			listISBN = App.getDB().sql("SELECT ISBN FROM livre").results(Livre.class);
		    		} 
		    		else {
		    			listISBN = App.getDB().sql("SELECT ISBN FROM livre WHERE ISBN LIKE ?", '%' + saisie5 + '%').results(Livre.class);
		    		}
		    		
		    		for(Livre livre : listISBN) {
		    			labelISBN = new Label();
		    			labelISBN.setText(String.valueOf(livre.ISBN));
		    			list.getChildren().add(labelISBN);
		    			
		    		}
		    	} 
		    }));
		    
		    Label label6 = new Label();
		    label6.setText("Date de publication");
		    TextField textfield6 = new TextField();
		    textfield6.setOnKeyPressed((new EventHandler<KeyEvent>() {
		    	@Override
		    	public void handle(KeyEvent event) {	
		    		Label labelDate;
		    		String saisie6 = textfield6.getText();
		    		List<Livre> listDate;
		    		scrollpane.setContent(list);
		    		list.getChildren().clear();
		    		
		    		if(saisie6.trim().isEmpty()) {
		    			listDate = App.getDB().sql("SELECT date_de_publication FROM livre").results(Livre.class);
		    		} 
		    		else {
		    			listDate = App.getDB().sql("SELECT date_de_publication FROM livre WHERE date_de_publication LIKE ?", '%' + saisie6 + '%').results(Livre.class);
		    		}
		    		
		    		for(Livre livre : listDate) {
		    			labelDate = new Label();
		    			labelDate.setText(String.valueOf(livre.date_de_publication));
		    			list.getChildren().add(labelDate);
		    			
		    		}
		    	} 
		    }));
		    
//		    Button btn = new Button();
//		    btn.setText("Chercher");
//		    Button btn2 = new Button();		    
		    Button btnAjout = new Button();
		    btnAjout.setText("Ajouter livre");
		    btnAjout.setOnAction(new EventHandler<ActionEvent>() {
		    	@Override
		    	public void handle(ActionEvent event) {	
    			    newLivre = new Livre();												
    			    newLivre.titre = textfield.getText();
    			    newLivre.auteur = textfield2.getText();
     			    newLivre.type_de_support = format.getSelectionModel().getSelectedItem().toString();
    			    newLivre.edition = textfield4.getText();
    			    newLivre.ISBN = Integer.valueOf(textfield5.getText());
    			    newLivre.date_de_publication = Date.valueOf(textfield6.getText());
    			    App.getDB().insert(newLivre);
    			    
    			    user2 = App.getDB().sql("SELECT id FROM utilisateur WHERE pseudo LIKE ?", name).first(Utilisateur.class);
    			    
    			    newPossede = new Possede();
    			    newPossede.id_livre = newLivre.getId();									
    			    newPossede.id_utilisateur = user2.id;
    			    App.getDB().insert(newPossede);
    			    JOptionPane.showMessageDialog(null, "Livre ajouté à votre bibliothèque", "Ajout livre", JOptionPane.WARNING_MESSAGE);
		    	}    			   
		    });
		    Vbox2.getChildren().addAll(label, textfield, label2, textfield2, label3, format, label4, textfield4, label5, textfield5, label6, textfield6/*, btn*/, btnAjout, scrollpane);	   
		 }
		
		//where the user saves his friends
		if(l.equals("Recherche ami")) {
		    lbl.setText("Recherche ami");
		    Vbox2.getChildren().clear();
		    
		    list = new VBox();
    		ScrollPane scrollpane = new ScrollPane();
    		list.getChildren().clear();
    		
		    Label label = new Label();
		    label.setText("Nom");
		    TextField textfield = new TextField();
		    textfield.setOnKeyPressed((new EventHandler<KeyEvent>() {
		    	@Override
		    	public void handle(KeyEvent event) {	
		    		Label labelNoms;
		    		String saisie = textfield.getText();
		    		List<Utilisateur> listNoms;
		    		scrollpane.setContent(list);
		    		list.getChildren().clear();
		    		
		    		if(saisie.trim().isEmpty()) {
		    			listNoms = App.getDB().sql("SELECT nom FROM utilisateur").results(Utilisateur.class);
		    		} 
		    		else {
		    			listNoms = App.getDB().sql("SELECT nom FROM utilisateur WHERE nom LIKE ?", '%' + saisie + '%').results(Utilisateur.class);
		    		}
		    		
		    		for(Utilisateur utilisateur : listNoms) {
		    			labelNoms = new Label();
		    			labelNoms.setText(String.valueOf(utilisateur.nom));
		    			list.getChildren().add(labelNoms);
		    			
		    		}
		    	} 
		    }));
		    Label label2 = new Label();
		    label2.setText("Prénom");
		    TextField textfield2 = new TextField();
		    textfield2.setOnKeyPressed((new EventHandler<KeyEvent>() {
		    	@Override
		    	public void handle(KeyEvent event) {	
		    		Label labelPrenoms;
		    		String saisie2 = textfield2.getText();
		    		List<Utilisateur> listPrenoms;
		    		scrollpane.setContent(list);
		    		list.getChildren().clear();
		    		
		    		if(saisie2.trim().isEmpty()) {
		    			listPrenoms = App.getDB().sql("SELECT prenom FROM utilisateur").results(Utilisateur.class);
		    		} 
		    		else {
		    			listPrenoms = App.getDB().sql("SELECT prenom FROM utilisateur WHERE prenom LIKE ?", '%' + saisie2 + '%').results(Utilisateur.class);
		    		}
		    		
		    		for(Utilisateur utilisateur : listPrenoms) {
		    			labelPrenoms = new Label();
		    			labelPrenoms.setText(String.valueOf(utilisateur.prenom));
		    			list.getChildren().add(labelPrenoms);
		    			
		    		}
		    	} 
		    }));
		    Label label3 = new Label();
		    label3.setText("Pseudo");
		    TextField textfield3 = new TextField();
		    textfield3.setOnKeyPressed((new EventHandler<KeyEvent>() {
		    	@Override
		    	public void handle(KeyEvent event) {	
		    		Label labelPseudos;
		    		String saisie3 = textfield3.getText();
		    		List<Utilisateur> listPseudos;
		    		scrollpane.setContent(list);
		    		list.getChildren().clear();
		    		
		    		if(saisie3.trim().isEmpty()) {
		    			listPseudos = App.getDB().sql("SELECT pseudo FROM utilisateur").results(Utilisateur.class);
		    		} 
		    		else {
		    			listPseudos = App.getDB().sql("SELECT pseudo FROM utilisateur WHERE pseudo LIKE ?", '%' + saisie3 + '%').results(Utilisateur.class);
		    		}
		    		
		    		for(Utilisateur utilisateur : listPseudos) {
		    			labelPseudos = new Label();
		    			labelPseudos.setText(String.valueOf(utilisateur.pseudo));
		    			list.getChildren().add(labelPseudos);
		    			
		    		}
		    	} 
		    }));
		    
		   
		    Button btn = new Button();
		    btn.setText("Ajouter ami");
		    btn.setOnAction(new EventHandler<ActionEvent>() {
		    	@Override             														
      	    	public void handle(ActionEvent event) {	  
		    		    String nom = textfield.getText();
		    		    String prenom = textfield2.getText();
		    		    String pseudo = textfield3.getText();
		   
		    			Utilisateur ami = App.getDB().sql("SELECT * FROM utilisateur WHERE nom = ? OR prenom = ? OR pseudo = ?", nom, prenom, pseudo).first(Utilisateur.class);
		    		
		    			if (nom.trim().isEmpty() || prenom.trim().isEmpty() || pseudo.trim().isEmpty()) {
		    				System.out.println("pseudo et/ou nom et/ou prénom erroné(s)");
		    			}
		    			if (ami == null) {
		    				System.out.println("pseudo et/ou nom et/ou prénom erroné(s)");
		    			}
		    			else if (ami !=  null) {
		    				newAmitie = new Amis();
		    				newAmitie.id_utilisateur = user.id;
		    				newAmitie.id_ami = ami.id;
		    				newAmitie.statut = "demande";
		    				App.getDB().insert(newAmitie);
		    			}
		    	}
		    });		    
		    
		    Vbox2.getChildren().addAll(label, textfield, label2, textfield2, label3, textfield3, btn, scrollpane);
		       
		}
		
		//where the user accepts the sharing of his library
		if(l.equals("Acceptation ami")) {
		    lbl.setText("Acceptez-vous de partager votre bibliothèque avec :");    
		    Vbox2.getChildren().clear();
		    
		    List<Utilisateur> demandeAmis = App.getDB().sql("SELECT utilisateur.id, nom, prenom, pseudo FROM utilisateur JOIN est_ami ON est_ami.id_utilisateur = utilisateur.id WHERE statut = 'demande'").results(Utilisateur.class);
		//    List<Utilisateur> demandeurs = App.getDB().sql("SELECT u.nom, u.prenom, u.pseudo FROM utilisateur as u WHERE id = ?", demandeAmis).results(Utilisateur.class); 
		   
		    Amis amitie = App.getDB().sql("SELECT ami.id_utilisateur, ami.id_ami FROM est_ami as ami WHERE statut = 'demande'").first(Amis.class);
		    Integer id_demandeur = amitie.id_utilisateur;	 	
		    Integer id_ami = amitie.id_ami;  
		    
		    if(demandeAmis != null && user.id == id_ami) {
		    	for(Utilisateur utilisateur : demandeAmis) {
		    		Label label = new Label();
			    	label.setText("Nom : \n" + "Prénom : \n" + "Pseudo : ");    
			    	label.setText((utilisateur.nom) + "\t" + (utilisateur.prenom)  + "\talias " + (utilisateur.pseudo));
			    	Button btn = new Button();
				    btn.setText("OUI");
				    Button btn2 = new Button();
				    btn2.setText("NON");
				    btn.setOnAction(new EventHandler<ActionEvent>() {
						   @Override
						   public void handle(ActionEvent event) {  
							nouvelleAmitie = new Amis();
					    	nouvelleAmitie.id_utilisateur = user.id; 
					    	nouvelleAmitie.id_ami = utilisateur.id; 
					    	nouvelleAmitie.statut = "ami";
		    				App.getDB().insert(nouvelleAmitie);
		    				Amis ancienneAmitie = App.getDB().where("statut = 'demande' AND id_utilisateur = ? AND id_ami = ?", utilisateur.id, user.id).first(Amis.class);
		    				ancienneAmitie.statut = "ami";
		    				App.getDB().update(ancienneAmitie);
						   }
					   });
				    Vbox2.getChildren().addAll(label, btn, btn2);
		    	}
		    }    
		}
	}
}	