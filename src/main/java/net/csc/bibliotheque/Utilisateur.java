package net.csc.bibliotheque;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.*;


@Table(name = "utilisateur")
public class Utilisateur {
	
	//class with the data of a user
	@Id
	@GeneratedValue
	public Integer id;
	
	public String nom;
	
	public String prenom;
	
	public String pseudo;
	
	public String mot_de_passe;

}


