package net.csc.bibliotheque;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class Click implements EventHandler<ActionEvent>{
	
	//class with click method
	public Integer id_utilisateur;
	public Integer id_livre;
	
	public Click (int id_utilisateur, Integer id_livre) {
		this.id_utilisateur = id_utilisateur;
		this.id_livre = id_livre;
	}
    	@Override
    	public void handle(ActionEvent event) {	
    		
    		App.getDB().sql("DELETE FROM possede WHERE id_utilisateur = ? AND id_livre = ?", id_utilisateur, id_livre).execute();
    	}
}
