module net.csc.bibliotheque {
    requires javafx.controls;
    requires javafx.fxml;
	requires javafx.graphics;
	requires java.sql;
	requires norm;
	requires persistence.api;
	requires javafx.base;
	requires java.desktop;

    opens net.csc.bibliotheque to javafx.fxml;
    exports net.csc.bibliotheque;
}